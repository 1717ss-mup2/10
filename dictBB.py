#!/usr/bin/env python3

# author: Leonard Krause

import re

'''
1:’forename’:’John’, ’surname’:’Doe’, ’job’:’Actor’, ’age’:42
'''

def addUser(dictDB, string):
    strings = re.split(';',string)
    user = {'forename':strings[1],'surname': strings[2],'job': strings[3],'age': int(strings[4])}
    dictDB[int(string[0])] = user;
    return dictDB

def readFile(dictDB, fileName):
    file = open(fileName, 'r')
    lines = file.readlines()
    for line in lines:
        addUser(dictDB,line)
    return dictDB

def printUser(dictDB, id):
    output = dictDB[id]['forename'] + ' ' + dictDB[id]['surname']
    print (output)

def removeUser(dictDB, id):
    dictDB[id] = None
    return dictDB

def removeUserAttribute(dictDB, id):
    del dictDB[id]
    return dictDB

dictDB = {}
readFile(dictDB,'dictionaryDB.txt')
removed = []
for i,user in enumerate(dictDB):
    if dictDB[user]['job'] == 'Mathematiker':
        removed.append(user)
        removeUser(dictDB,user)
for id in removed:
    dictDB = removeUserAttribute(dictDB,id)
for i,user in enumerate(dictDB):
    if dictDB[user]['age'] == 23:
        printUser(dictDB,user)
