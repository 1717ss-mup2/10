#!/usr/bin/env python3

# author: Leonard Krause

def uniqueOld(liste):
    '''
        len(liste) zeit : O(1); speicher: O(1)
        set(liste) zeit : O(n); speicher: O(n)
    '''
    return len(liste) == len(set(liste))

def unique(liste):
    i = 0;
    for i, item in enumerate(liste):
        if liste[abs(item)] >= 0:
            liste[abs(item)] = -liste[abs(item)];
        else:
            return True
    return False

tests = {
    1:[ 3, 1, 2, 0],
    2:[ 3, 1, 2, 0, 1],
    3:[ 1,2,3,4,5,6,1,2,3,4]
}

for key in tests:
    print ('test {}: unique({}) = {}'.format(key,tests[key],unique(tests[key])))
