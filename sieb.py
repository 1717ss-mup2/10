#!/usr/bin/env python3

# author: Leonard Krause

'''
based on https://gitlab.com/1717ss-mup2/6/blob/5acf719d5f5a251a9a649c9db4e998a18fdff30a/sieb.cl
'''
def createSieb(n):
    sieb = []
    if n < 2: return sieb
    for i in range(2,n+1):
        sieb.append(i)
    return sieb


def sieb(n):
    sieb = createSieb(n)
    result = []
    masks = []
    for i in range(2,n+1):
        masks.append(sieb[i-2::i]) # FIXME geht noch effizienter, wobei die masken schnell relativ klein werden
    for mask in masks:
        if (len(sieb) > 0 and (len(result) < 1 or result[-1] != sieb[0])): result.append(sieb[0])
        sieb = [i for i in sieb if i not in mask]
    return result

tests = {
    0: 10,
    1: 100,
    2: 1000
}

for key in tests:
    print ('test {}: sieb({}) = {}'.format(key,tests[key],sieb(tests[key])))
